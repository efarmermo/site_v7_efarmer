<?php
/**
 * Header Login Content
 *
 * @since MX 1.0
 */

?>

<?php if (mx_get_options_key('global-login-enable') == "on") { ?>
    <?php
    if (class_exists('woocommerce')) {
        global $woocommerce;
        if (is_user_logged_in()) {
            global $userdata;
            ?>
            <a href="#" class="log-in" onclick="setVisibility('enterApp', 'block');">
                Log in
            </a>
            <div class="user-contents-container" id="enterApp">
                <div class="row">
                    <div class="col-xs-5 col-xs-offset-1"><a href="http://efarmer.mobi:8080/eFarmer/login.km" class="green-link" target="_blank"> Log in to old version </a></div>
                    <div class="col-xs-5"><a href="http://efarmer.mobi:8080/efarmer" class="green-link" target="_blank"> Log in to new version </a></div>
                </div>
            </div>
            <?php
        } else {
            ?>
            <a  href="http://efarmer.mobi:8080/efarmer"  target="_blank" class="log-in"  >
                Log in
            </a>
            </div>
            <?php
        }
    } else {
        if (is_user_logged_in()) {
            ?>
            <?php
        } else {
            ?>
            <p><a href=""> Log in to  </a></p>
            <p><a href=""></a></p>
            <?php
        }
    }
    ?>

<script>
function setVisibility(id, visibility) {
if(document.getElementById(id).style.display === visibility) {
document.getElementById(id).style.display = 'none';
} else {
document.getElementById(id).style.display = visibility;
}
}
</script>

<?php } ?>
