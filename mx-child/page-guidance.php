<?php
/**
 * Template Name: Tractor Guidance
 *
 * @since MX 1.0
 */
get_header();

// get page layout
$layout = mx_get_page_layout();
$layout_class = mx_get_page_layout_class();

?>
    <div id="main">
        <div class="submenu hidden-sm hidden-xs">
            <div class="container">
                <div class="row" style="padding: 10px 0; border-top: 3px solid #ffffff;">
                    <p class="font-little light-grey light-weight col-sm-4 hidden-xs">Navigation solutions</p>

                    <div class="col-sm-8">
                        <ul class="list-inline font-little light-weight" style="position: relative; float:right">
                            <li class="text-left"><a href="#prices" class="light-grey">Prices</a>
                            </li>
                            <li class="text-left"><a href="#naviAppLink"
                                                     class="light-grey ">Navi App</a></li>
                            <li class="text-left"><a href="#naviHighLink"
                                                     class="light-grey ">Navi 20 cm</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="hidden-sm hidden-xs placeholder"></div>
        <div class="header-bg-guidance ">
            <div class="container ">
                <div class=" row">

                    <div class="col-md-5 col-sm-6" style="padding-top:90px">
                        <h1 class="main-color normal-weight font-large">eFarmer Navi - Navigation solutions</h1>

                        <h3 class="main-color light-weight font-middle">Making every tractor a smart tractor</h3>

                        <p class="main-color light-weight font-little padding-both-30">Professional tractor guidance
                            system on your own smartphone and tablet</p>

                        <div class="row" style="padding-bottom:90px">
                            <div class="col-md-4 col-xs-5 ">
                                <button type="button" class="btn btn-default btn-trial" aria-label="Left Align"
                                        style="background-color: #95a1aa">
                                    <a href="https://play.google.com/store/apps/details?id=com.kmware.efarmer&hl=ru"
                                       target="_blank">FREE TRIAL</a>
                                </button>
                            </div>
                            <div class="col-md-4 col-md-offset-1 col-xs-5 col-xs-offset-1">
                                <button type="button" class="btn btn-default btn-buy" aria-label="Left Align"
                                        style="background-color: #5d656e">
                                    <a href="http://localhost:8888/efarmer/shop/">buy now</a>
                                </button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="row opposite-bg">
            <div class="container padding-both-30 arrow_box opposite-bg">
                <div class="row">
                    <div class="col-sm-1 col-xs-12 text-center">
                        <div class="white-circle margin-top-45">
                            <i class="material-icons green-icon opposite-text">folder</i>
                        </div>
                    </div>
                    <div class="col-sm-10 col-xs-12">
                        <div class="row">
                            <div class="col-xs-10 col-xs-offset-1">
                                <h2 class="text-center white-text font-32 normal-weight">
                                    <span
                                        class="underline">More than tractor gudiance</span>
                                </h2>

                                <p class="text-center white-text font-22 light-weight">eFarmer uniquely combines
                                    tractor GPS guidance with record keeping and farm management. Check the detailed
                                    description of what
                                    you can do
                                    with our navigation solutions: </p>
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-1 col-xs-12 text-center">
                        <div class="white-circle margin-top-45">
                            <i class="material-icons green-icon opposite-text">navigation</i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row" style="margin-top: 30px">

                <div class="col-md-4 col-sm-6 col-xs-12 ">
                    <div class="features-block text-center">
                        <div class="green-circle">
                            <i class="material-icons white-icon">map</i>
                        </div>
                        <h4 class="normal-weight grey-37  font-20">Maps </h4>

                        <p class="light-weight grey-37 ">Create and manage field boundaries. Store information and view the
                            progress of
                            your work on multiple fields. </p>
                        <a href="#" class="opposite-text">Learn more </a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12 ">
                    <div class="features-block text-center">
                        <div class="green-circle">
                            <i class="material-icons white-icon">library_books</i>
                        </div>
                        <h4 class="normal-weight grey-37  font-20">Field records </h4>

                        <p class="light-weight grey-37 ">Make records of your fieldwork directly in the field. Save
                            time on paperwork. Store and analyse information about your work. Export, share & print
                            field reports. </p>
                        <a href="#" class="opposite-text">Learn more </a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12 ">
                    <div class="features-block text-center">
                        <div class="green-circle">
                            <i class="material-icons white-icon">navigation</i>
                        </div>
                        <h4 class="normal-weight grey-37  font-20">Tractor guidance </h4>

                        <p class="light-weight grey-37 ">Driving display hints will enable you to work in parallel and
                            evenly spaced lines. Thus reducing overlaps and missed spots. </p>
                        <a href="#" class="opposite-text">Learn more </a>
                    </div>
                </div>


                <div class="col-md-4 col-sm-6 col-xs-12 ">
                    <div class="features-block text-center">
                        <div class="green-circle">
                            <i class="material-icons white-icon">devices</i>
                        </div>
                        <h4 class="normal-weight grey-37  font-20">Mobile and web</h4>

                        <p class="light-weight grey-37 ">Synchronize data between all your devices. Access your data conveniently from anywhere. </p>
                        <a href="#" class="opposite-text">Learn more </a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12 ">
                    <div class="features-block text-center">
                        <div class="green-circle">
                            <i class="material-icons white-icon">cloud_queue</i>
                        </div>
                        <h4 class="normal-weight grey-37  font-20">Secure cloud</h4>

                        <p class="light-weight grey-37 ">All your data is securely saved in the cloud. </p>
                        <a href="#" class="opposite-text">Learn more</a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12 ">
                    <div class="features-block text-center">
                        <div class="green-circle">
                            <i class="material-icons white-icon">settings_input_antenna</i>
                        </div>
                        <h4 class="normal-weight grey-37  font-20">External receiver</h4>

                        <p class="light-weight grey-37 ">Reduce the width of overlaps down to 20 cm with high
                            positioning accuracy with our GPS receiver.</p>
                        <a href="#" class="opposite-text">Learn more </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="price-bg padding-both-30">
            <span id="prices" class="link-document"> &nbsp;</span>

            <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <p class="text-center "> <span class="underline-dark font-32" style="border-color: #95a1aa">
                                eFarmer navigation solutions and prices </span>
                        </p>

                        <p class="dark-grey-color font-22 text-center light-weight">
                            When making a decision on which navigation system to purchase, you should consider Return on
                            Investment.
                            With higher accuracy you should get your money back faster. We offer receivers with the
                            following levels of accuracy: </p>
                    </div>
                </div>
                <div class="row text-center">
                    <div class="col-xs-12 text-center">
                        <button type="button" class="btn btn-period-select active">
                            One - off
                        </button>
                    </div>
<!--                    <div class="col-xs-12 text-center">-->
<!--                        <button type="button" class="btn btn-period-select active" id="oneOff">-->
<!--                            One - off-->
<!--                        </button>-->
<!--                    </div>-->
<!--                    <div class="col-xs-6 text-left">-->
<!--                        <button type="button" class="btn btn-period-select" id="subscription">-->
<!--                            Subscription-->
<!--                        </button>-->
<!--                    </div>-->
                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="price-block app free-block">
                            <h4 class="text-center bold-weight font-20 white-text shadow">Tractor guidance trial</h4>

                            <p class="text-center price-label bold-weight shadow">FREE</p>

                            <p class="text-center note-text">10 day free trial period</p>

                            <div class="padding-both-30">
                                <ul>
                                    <li>All Navi features</li>
                                    <li>Your device <u>accuracy </u> <sup>*</sup></li>
                                    <li>Compatible with Novatel AG-Star&#8482; and Ublox&#8482; based receivers</li>
                                </ul>
                            </div>
                            <div class="abs-pos">
                                <p class="text-center "><a href="#" class="note-text" data-toggle="modal"
                                                           data-target="#NaviAppTrialModal">More info</a>
                                </p>
                                <button class="btn btn-action-select"
                                        type="submit">
                                    <a href="https://play.google.com/store/apps/details?id=com.kmware.efarmer&hl=ru"
                                       target="_blank">try now</a>
                                </button>
                            </div>
                        </div>


                        <!-- START modal Navi App Trial-->
                        <div class="modal fade" tabindex="-1" id="NaviAppTrialModal" role="dialog">
                            <div class="modal-dialog modal-lg" style="margin-top: 200px">
                                <div class="modal-content row">
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <div class="price-block app free-block modal-price">
                                            <h4 class="text-center bold-weight font-20 white-text shadow ">Tractor
                                                guidance trial</h4>

                                            <p class="text-center price-label bold-weight shadow">FREE</p>

                                            <p class="text-center note-text">10 day free trial period</p>

                                            <div class="padding-both-30">
                                                <ul>
                                                    <li>All Navi features</li>
                                                    <li>Your device <u>accuracy </u>
                                                        <sup>*</sup></li>
                                                    <li>Compatible with Novatel AG-Star&#8482; and Ublox&#8482; based receivers</li>
                                                </ul>
                                            </div>
                                            <div class="abs-pos modal-price">
                                                <button class="btn btn-action-select"
                                                        type="submit">
                                                    <a href="https://play.google.com/store/apps/details?id=com.kmware.efarmer&hl=ru"
                                                       target="_blank">try now</a>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-sm-6 col-xs-12">
                                        <p class="font-middle grey-37 margin-both-20">10 day free trial
                                            period</p>

                                        <p class="simple-p margin-both-20"> What’s included: <span
                                                class="dark-grey-color "> eFarmer navigation software with all functionality </span>
                                        </p>

                                        <p class="simple-p margin-both-20"><sup>*</sup>Accuracy determined by your own
                                            device .  <span class="dark-grey-color"> The app can operate using the
                                            inbuilt GPS of your Smartphone / tablet but location accuracy may be low and
                                            as such the use may be impaired, particularly during the in - cabin use. If
                                            available you can use your own external receiver . The accuracy will be
                                            determined by its unique characteristics. Check the
                                                <a href="https://efarmer.atlassian.net/wiki/display/SUPP/eFarm+Pilot+GPS+Field+Guidance+Compatible+Devices+List" target="_blank" class="link-devices"> overview of tested devices and receivers. </a>
                                                </span>
                                        </p>

                                        <p class="simple-p margin-both-20"> You can order additional devices::
                                            <span
                                                class="dark-grey-color">navigation software, external receiver,
                                            compatible tablet, holders, chargers, cables and other peripherals . </span>
                                        </p>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END modal Navi App Trial-->


                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="price-block app middle-price-block">
                            <h4 class="text-center bold-weight font-20 white-text shadow">Navi App</h4>

                            <p class="text-center price-label bold-weight shadow"
                               id="naviApp">€ 249</p>

                            <p id="naviAppSubscription"
                               style="display: none;">€ 97</p>

                            <p class="text-center note-text"
                               id="naviAppDesc">tractor guidance license</p>

                            <p style="display: none"
                               id="naviAppDescSub">for one year subscription</p>

                            <div class="padding-both-30">
                                <ul>
                                    <li>Regular app updates</li>
                                    <li>Your device <u>accuracy </u> <sup>*</sup></li>
                                    <li> Compatible with Novatel AG-Star&#8482; and Ublox&#8482; based receivers
                                    </li>
                                </ul>
                            </div>
                            <div class="abs-pos">
                                <p class="text-center "><a href="#" class="note-text " data-toggle="modal"
                                                           data-target="#NaviAppModal">More info</a>
                                </p>
                                <button class="btn btn-action-select">
                                    <a href="http://localhost:8888/efarmer/shop/">try now</a>
                                </button>
                            </div>
                        </div>

                        <!-- START modal Navi App -->
                        <div class="modal fade" tabindex="-1" id="NaviAppModal" role="dialog">
                            <div class="modal-dialog modal-lg" style="margin-top: 200px">
                                <div class="modal-content row">
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <div class="price-block app middle-price-block modal-price">
                                            <h4 class="text-center bold-weight font-20 white-text shadow ">Navi App</h4>

                                            <p class="text-center price-label bold-weight shadow"
                                               id="naviAppPriceModal"></p>

                                            <p class="text-center note-text"
                                               id="naviAppDescModal">tractor guidance license</p>

                                            <div class="padding-both-30">
                                                <ul>
                                                    <li>Regular app updates</li>
                                                    <li>Your device <u>accuracy </u>
                                                        <sup>*</sup></li>
                                                    <li>Compatible with Novatel AG-Star&#8482; and Ublox&#8482; based receivers 
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="abs-pos modal-price">
                                                <button class="btn btn-action-select"
                                                        type="submit">
                                                    <a href="http://localhost:8888/efarmer/shop/">try now</a>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-sm-6 col-xs-12">
                                        <p class="font-middle grey-37 margin-both-20">eFarmer Navi software</p>

                                        <p class="simple-p margin-both-20"> What’s included: <span
                                                class="dark-grey-color "> eFarmer navigation software with all functionality and regular updates </span>
                                        </p>

                                        <p class="simple-p margin-both-20" id="priceNaviAppModal"> Price: <span
                                                class="dark-grey-color"> 249 Euro one off payment </span>
                                        </p>

                                        <p class="simple-p margin-both-20 no-view" id="priceNaviAppSubsModal">
                                            Price: <span
                                                class="dark-grey-color"> 97 EUR/year for Navi software </span>
                                        </p>

                                        <p class="simple-p margin-both-20"><sup>*</sup>Accuracy determined by your own
                                            device.  <span class="dark-grey-color"> The app can operate using the
                                            inbuilt GPS of your Smartphone / tablet but location accuracy may be low and
                                            as such the use may be impaired, particularly during the in - cabin use. If
                                            available you can use your own external receiver . The accuracy will be
                                            determined by its unique characteristics. Check the <a
                                                    href="https://efarmer.atlassian.net/wiki/display/SUPP/eFarm+Pilot+GPS+Field+Guidance+Compatible+Devices+List" target="_blank"
                                                    class="link-devices">
                                                    overview of tested devices and receivers.</a>
                                                </span>
                                        </p>


                                        <p class="simple-p margin-both-20">You can order additionally: 
                                            <span
                                                class="dark-grey-color">external receiver, compatible tablet, holders, chargers, cables and other peripherals . </span>
                                        </p>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END modal Navi App -->

                    </div>

                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="price-block app high-price-block">
                            <h4 class="text-center bold-weight font-20 white-text shadow">Navi 20 cm</h4>

                            <p class="text-center price-label bold-weight shadow"
                               id="naviHigh">€ 1249</p>

                            <p id="naviHighSubscription"
                               style="display: none;">€ 999</p>

                            <p class="text-center note-text"
                               id="naviHighDesc">incl. free tractor guidance license</p>

                            <p style="display: none"
                               id="naviHighDescSub">+ € 97/year for Navi app subscription</p>


                            <div class="padding-both-30">
                                <ul>
                                    <li>Regular app updates</li>
                                    <li> <span data-toggle="tooltip" data-placement="right"
                                               title="according to Landwirtschaftskammer of Nordrhein-Westfalen"
                                               class="white-tooltip">
                                           <u>€ 13 / ha savings</u> <sup>**</sup></span></li>
                                    <li>20 cm <u>accuracy </u> <sub>*</sub></li>
                                    <li>External GPS receiver</li>
                                    <li>Free shipping in EU</li>
                                </ul>
                            </div>
                            <div class="abs-pos">
                                <p class="text-center "><a href="#" class="note-text" data-toggle="modal"
                                                           data-target="#NaviHighModal">
                                        <img src="../wp-content/themes/mx-child/fonts/iconfont/ic_calc.svg"
                                             class="icon-calc "/>

                                        Calculate your saving</a></p>
                                <button class="btn btn-action-select"
                                        type="submit">
                                    <a href="http://localhost:8888/efarmer/shop/">buy now</a>

                                </button>
                            </div>

                        </div>

                        <!-- START modal Navi High-->
                        <div class="modal fade" tabindex="-1" id="NaviHighModal" role="dialog">
                            <div class="modal-dialog modal-lg" style="margin-top: 200px">
                                <div class="modal-content row">
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <div class="price-block app high-price-block modal-price">
                                            <h4 class="text-center bold-weight font-20 white-text shadow ">Navi 20
                                                cm</h4>

                                            <p class="text-center price-label bold-weight shadow"
                                               id="naviHightPriceModal"></p>

                                            <p class="text-center note-text"
                                               id="naviHighDescModal">incl. free tractor guidance license</p>

                                            <div class="padding-both-30">
                                                <ul>
                                                    <li>Regular app updates</li>
                                                    <li>€ 13 / ha savings</li>
                                                    <li>20 cm <u>accuracy </u>
                                                        <sup>*</sup></li>
                                                    <li>External GPS receiver</li>
                                                    <li>Free shipping in EU</li>
                                                </ul>
                                            </div>
                                            <div class="abs-pos modal-price">
                                                <button class="btn btn-action-select"
                                                        type="submit">
                                                    <a href="http://localhost:8888/efarmer/shop/">buy now</a>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-sm-6 col-xs-12">
                                        <p class="font-middle grey-37 margin-both-20">eFarmer Navi software + receiver
                                            20
                                            cm</p>

                                        <div
                                            class="simple-p margin-both-20">What’s included:

                                            <ul class="">
                                                <li class="dark-grey-color">eFarmer navigation software with regular
                                                    updates
                                                </li>
                                                <li class="dark-grey-color">GNSS receiver to secure stability + external
                                                    receiver with accuracy up to 20 cm
                                                </li>
                                            </ul>
                                        </div>

                                        <p class="simple-p margin-both-20" id="priceNaviHighModal"> Price: <span
                                                class="dark-grey-color "> 1249 Euros(1000 EUR for receiver + 249 EUR for Navi software)</span>
                                        </p>

                                        <p class="simple-p margin-both-20 no-view"
                                           id="priceNaviHighSubsModal"> Price: <span
                                                class="dark-grey-color "> 999 EUR for receiver + 97 EUR/year for Navi software </span>
                                        </p>

                                        <p class="simple-p margin-both-20"> Average savings:  <span
                                                class="dark-grey-color "> 13 Euros per Ha .
<!--                                            <a href="#" class="green-link">Calculate your savings . </a> -->
                                            </span>
                                        </p>

                                        <p class="simple-p margin-both-20"><sup>*</sup>Accuracy: <span
                                                class="dark-grey-color"> Up to 20 cm of maximum discrepancy level . In average the accuracy is about 10 - 20 cm .
                                                </span>
                                        </p>

                                        <p class="simple-p margin-both-20">You can order additionally:
                                            <span
                                                class="dark-grey-color">Compatible tablet, holders, chargers, cables and other peripherals . </span>
                                        </p>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END modal Navi High-->


                    </div>
                </div>
                <p class="simple-p light-weight">Tractor GPS guidance system allows you to increase efficiency of your
                    farm by
                    saving materials, working hours and machine costs . </p>
            </div>
        </div>
        <div class="row opposite-bg">
            <span id="naviAppLink" class="link-document"> &nbsp;</span>

            <div class="container padding-both-30 arrow_box opposite-bg">
                <div class="row">

                    <div class="col-sm-12">
                        <h2 class="text-center white-text font-32 normal-weight"><span
                                class="underline">Navigation App</span>
                        </h2>

                        <p class="text-center white-text font-22 light-weight">Navigation without the external
                            receiver</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 padding-both-30 light-weight">
                    <p class="text-center font-20 grey-37">The eFarmer navigation software is developed to be used on your
                        own Android smartphone and tablet. The functionality of the application combines the tractor GPS
                        guidance with farm management.
                        Check the detailed description of the application:</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-6 app-screens-block">
                    <div class="maps-img">
                    </div>
                    <p class="font-20 grey-37 text-center">Maps</p>

                    <p class="text-center simple-p light-weight">Create and manage field boundaries. </p>
                </div>
                <div class="col-md-4 col-sm-6 app-screens-block">
                    <div class="farm-records-img">
                    </div>
                    <p class="font-20 grey-37 text-center">Farm records</p>

                    <p class="text-center simple-p light-weight">Make records of your field work directly in the field
                        . </p>
                </div>
                <div class="col-md-4 col-sm-6 app-screens-block">
                    <div class="tractor-guidance-img">
                    </div>
                    <p class="font-20 grey-37 text-center">Tractor guidance</p>

                    <p class="text-center simple-p light-weight">Drive in parallel and evenly spaced lines. </p>
                </div>
                <div class="col-md-4 col-sm-6 app-screens-block">
                    <div class="mobile-web-img">
                    </div>
                    <p class="font-20 grey-37 text-center">Mobile and web app</p>

                    <p class="text-center simple-p light-weight">Synchronise data between all your devices. </p>
                </div>
                <div class="col-md-4 col-sm-6 app-screens-block">
                    <div class="secure-cloud-img">
                    </div>
                    <p class="font-20 grey-37 text-center">Secure cloud</p>

                    <p class="text-center simple-p light-weight">All your data is securely saved in the cloud. </p>
                </div>
                <div class="col-md-4 col-sm-6 app-screens-block">
                    <div class="much-more-img">
                    </div>
                    <p class="font-20 grey-37 text-center"> And much more</p>

                    <p class="text-center simple-p light-weight">Checkout the Features page to know all the
                        details </p>
                </div>
            </div>
        </div>
        <div class="all-devices-bg arrow_box">
            <div class="container">
                <p class="font-22 grey-37 text-center padding-both-30">The GPS navigation system allows you to increase the
                    efficiency of your farm by saving materials,
                    working hours and machine costs. </p>

                <div class="row">
                    <div class="col-sm-6">
                        <p class="dark-grey-color small-text-p">
                            After purchasing the eFarmer navigation software you will be able to install it on your device . In
                            this case the accuracy of the system will be determined by your phone or the tablet internal
                            GPS receiver
                            and its stability. This highly varies from device to device and can be particularly impaired
                            during the in - cabin work. We are constantly test new gadgets; you can find the list of
                            the
                            best gadgets which can be used with the eFarmer navigation system
                            <a href="https://efarmer.atlassian.net/wiki/display/SUPP/eFarm+Pilot+GPS+Field+Guidance+Compatible+Devices+List" target="_blank" class="green-link">
                                here</a>.
                        </p>

                        <p class="dark-grey-color small-text-p">To improve the accuracy of the system we advise you to
                            use an external receiver . If you have
                            already one, you can use it with our software . You might need some cables and other
                            peripherals to do so . Please <a href="#" data-toggle="modal" data-target="#contactUs"
                                                             class="green-link"> contact our Support </a> and we
                            will
                            be happy to assist you, as using eFarmer is easy and can be done by everyone .
                        </p>
                    </div>
                    <div class="col-sm-6">
                        <img src="../wp-content/uploads/2015/10/tablet-guidance.png" class="tablet-pic" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="grey-gradient-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <img src="../wp-content/uploads/2015/10/360-SMART-AG.png" class="antenna-pic" alt="">

                        <div class="text-center row" style="padding-bottom: 30px">
                            <button type="button" class="btn btn-default btn-buy" aria-label="Left Align">
                                <a href="http://localhost:8888/efarmer/shop/">SHOP NOW</a>
                            </button>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <p class="font-22 white-text" style="margin: 50px 0">
                            The amount of savings directly depends on the accuracy of the GPS signal and its stability.
                        </p>

                        <p class="small-text-p white-text light-weight">To improve the accuracy of the system we advise
                            you to use an external receiver. If you have already one, you can use it with our software.
                            You might need some cables and other peripherals to do so . Please contact our Support and
                            will be happy to assist you, as using eFarmer is easy and can be done by everyone . </p>

                        <p class="small-text-p white-text light-weight">To increase your benefits from tractor guidance
                            system we offer navigation solutions that combine eFamer software with best receivers
                            according to their price and quality. Check out <a href="#naviHighLink"
                                                                                class="opposite-link"> Navigation system
                                with accuracy 20 cm.</a></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row blue-sky">
            <span id="naviHighLink" class="link-document"> &nbsp;</span>

            <div class="container padding-both-30 arrow_box blue-sky">
                <div class="row">

                    <div class="col-sm-12">
                        <h2 class="text-center white-text font-32 normal-weight"><span
                                class="underline">Navigation system 20 cm</span>
                        </h2>

                        <p class="text-center white-text font-22 light-weight">Navigation solution with accuracy 20 cm
                            is a combination of the eFarmer navigation software and receiver from Novatel. </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row padding-both-30">
                <div class="col-sm-4">
                    <h3 class="font-32 normal-weight blue-text text-center"> Navi App</h3>
                    <img src="../wp-content/uploads/2015/10/tablet-guidance-small.png" alt="" class="img-offet">

                    <p class="main-color font-little light-weight">Android app for tractor navigation and record
                        keeping. <a href="#naviAppLink"
                                    class="green-link">More info</a>
                    </p>
                </div>
                <div class="col-sm-4">
                    <span class="vertical-divider-20"></span>

                    <div class="plus-block">
                        <div class="grey-plus">
                            <i class="material-icons grey-plus-color">add</i>
                        </div>

                        <p class="main-color font-little ">GPS navigation system allows you to increase the efficiency of
                            your farm business by saving materials, working hours and machine costs . The amount of
                            savings directly depends on the accuracy of the system . </p>
<!--                        <button type="button"-->
<!--                                class="btn btn-light-grey">Calculate savings-->
<!--                        </button>-->
                    </div>

                    <span class="vertical-divider-40"></span>


                </div>
                <div class="col-sm-4">
                    <h3 class="font-32 normal-weight blue-text text-center">Novatel receiver</h3>
                    <img src="../wp-content/uploads/2015/10/360-SMART-AG-small.png" alt="" class="img-offet">

                    <ul class="img-offset light-weight main-color font-little">
                        <li>High quality and precision .</li>
                        <li><a href="http://www.novatel.com/products/smart-antennas/ag-star/" target="_blank"
                               class="green-link">Novatel Ag-Star (tm) receiver</a></li>
                        <li>Accuracy: up to 20 cm over 15 minutes</li>
                        <li>Bluetooth connection to your smartphone and tablet</li>
                        <li>Micro USB charger</li>
                    </ul>
                </div>
            </div>
            <hr>
            <p class="text-center font-20 main-color">We guarantee the quality of the system
                and provide you with <b> 1 year warranty </b> period for the receiver and <b> 90 days </b> for the
                peripherals .
            </p>

            <div class="row">
                <div class="border-block col-sm-10 col-sm-offset-1">
                    <button class="btn buy-btn blue-sky white-text">
                        <a href="http://localhost:8888/efarmer/shop/">BUY SOLUTION</a>
                    </button>
                    <button class="btn buy-btn grey-border white-text" data-toggle="modal" data-target="#contactUs">
                        CONTACT US
                    </button>
                </div>
            </div>
        </div>

        <div class="tractor-bg">
            <p class="font-32 text-center main-color padding-both-30">Affordable precision farming system. </p>

            <div class="row text-center">
                <div class="euro-pic-grey"></div>
                <div class="plus-pic"></div>
                <div class="like-pic-grey"></div>

            </div>

        </div>
        <div class="farm-building-bg">
            <p class="font-32 text-center main-color">Increase the efficiency of your farm business. </p>
        </div>
        <div class="easy-use-bg">
            <p class="font-32 text-center white-text shadow">Easy to install and use. <br>
                No need to pay dealers and installation fees. </p>
        </div>

        <!--                    START contact us modal-->
        <div class="modal fade" tabindex="-1" id="contactUs" role="dialog">
            <div class="modal-dialog modal-md" style="margin-top: 200px">
                <div class="modal-content row">
                    <div class="col-xs-12">
                        <div class="row green-bg contact">
                            <div class="">
                                <div class="row">
                                    <h2 class="text-center white-text font-22 normal-weight">Contact us
                                    </h2>
                                </div>
                            </div>
                        </div>

                        <div class="contacts-block">
                            <p class="font-little dark-grey-color light-weight"><i
                                    class="fa fa-envelope"></i> info@efarmer.mobi </p>

                            <p class="font-little dark-grey-color light-weight"><i
                                    class="fa fa-skype"></i>
                                eFarmer.Support</p>

                            <p class="font-little dark-grey-color light-weight"><i
                                    class="fa fa-phone"></i>
                                +31 713 020355</p>
                        </div>
                        <button class="btn btn-action-select contact"
                                type="submit" data-toggle="modal" data-target="#contactUs">
                            <a href="mailto:oridyy@gmail.com?Subject=Support">Send e-mail </a>
                        </button>

                    </div>
                </div>
            </div>
            <!--                    END contact us modal-->
        </div>
<?php get_footer(); ?>