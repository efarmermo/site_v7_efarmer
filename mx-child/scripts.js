/**
 * Created by madelgeek on 11/23/15.
 */
jQuery(document).ready(function ($) {

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });


    $('img.svg').each(function(){
        var $img = jQuery(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');

        $.get(imgURL, function(data) {
            // Get the SVG tag, ignore the rest
            var $svg = jQuery(data).find('svg');

            // Add replaced image's ID to the new SVG
            if(typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the new SVG
            if(typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass+' replaced-svg');
            }

            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');

            // Replace image with new SVG
            $img.replaceWith($svg);

        }, 'xml');
    });

    if (document.getElementById('naviApp')) {
        var naviApp = document.getElementById('naviApp').innerHTML;
        var naviAppSubscription = document.getElementById('naviAppSubscription').innerHTML;
        var naviAppDesc = document.getElementById('naviAppDesc').innerHTML;
        var naviAppDescSub = document.getElementById('naviAppDescSub').innerHTML;


        //var naviMIddle = document.getElementById('naviMIddle').innerHTML;
        //var naviMIddleSubscription = document.getElementById('naviMIddleSubscription').innerHTML;
        //var naviMIddleDesc = document.getElementById('naviMIddleDesc').innerHTML;
        //var naviMIddleDescSub = document.getElementById('naviMIddleDescSub').innerHTML;

        var naviHigh = document.getElementById('naviHigh').innerHTML;
        var naviHighSubscription = document.getElementById('naviHighSubscription').innerHTML;
        var naviHighDesc = document.getElementById('naviHighDesc').innerHTML;
        var naviHighDescSub = document.getElementById('naviHighDescSub').innerHTML;
    }

    var flag;


    $("#oneOff").click(function () {
        flag = 2;
        $(this).toggleClass("active");
        $("#subscription").toggleClass("active");
        document.getElementById('naviApp').innerHTML = naviApp.replace(naviApp, naviApp);
        document.getElementById('naviAppDesc').innerHTML = naviAppDesc.replace(naviAppDesc, naviAppDesc);

        //document.getElementById('naviMIddle').innerHTML = naviMIddle.replace(naviMIddle, naviMIddle);
        //document.getElementById('naviMIddleDesc').innerHTML = naviMIddleDesc.replace(naviMIddleDesc, naviMIddleDesc);

        document.getElementById('naviHigh').innerHTML = naviHigh.replace(naviHigh, naviHigh);
        document.getElementById('naviHighDesc').innerHTML = naviHighDesc.replace(naviHighDesc, naviHighDesc);


    });

    $("#subscription").click(function () {
        flag = 1;
        $(this).toggleClass("active");
        $("#oneOff").toggleClass("active");
        document.getElementById('naviApp').innerHTML = naviApp.replace(naviApp, naviAppSubscription);
        document.getElementById('naviAppDesc').innerHTML = naviAppDesc.replace(naviAppDesc, naviAppDescSub);

        //document.getElementById('naviMIddle').innerHTML = naviMIddle.replace(naviMIddle, naviMIddleSubscription);
        //document.getElementById('naviMIddleDesc').innerHTML = naviMIddleDesc.replace(naviMIddleDesc, naviMIddleDescSub);

        document.getElementById('naviHigh').innerHTML = naviHigh.replace(naviHigh, naviHighSubscription);
        document.getElementById('naviHighDesc').innerHTML = naviHighDesc.replace(naviHighDesc, naviHighDescSub);

    });

    $('#NaviAppModal').on('show.bs.modal', function () {
        var naviAppDescModal = document.getElementById('naviAppDescModal').innerHTML;
        if (flag === 1) {
            $("#naviAppPriceModal").html(naviAppSubscription);
            $('#priceNaviAppModal').addClass('no-view');
            $('#priceNaviAppSubsModal').removeClass('no-view');
            document.getElementById('naviAppDescModal').innerHTML = naviAppDesc.replace(naviAppDesc, naviAppDescSub);
        } else {
            $("#naviAppPriceModal").html(naviApp);
            $('#priceNaviAppSubsModal').addClass('no-view');
            $('#priceNaviAppModal').removeClass('no-view');
            document.getElementById('naviAppDescModal').innerHTML = naviAppDescModal.replace(naviAppDescModal, naviAppDesc);
        }
    });

    //$('#NaviMiddleModal').on('show.bs.modal', function () {
    //    var naviMIddleDescModal = document.getElementById('naviMIddleDescModal').innerHTML;
    //
    //    if (flag === 1) {
    //        $("#naviMiddlePriceModal").html(naviMIddleSubscription);
    //        document.getElementById('naviMIddleDescModal').innerHTML = naviMIddleDescModal.replace(naviMIddleDescModal, naviMIddleDescSub);
    //
    //    } else {
    //        $("#naviMiddlePriceModal").html(naviMIddle);
    //        document.getElementById('naviMIddleDescModal').innerHTML = naviMIddleDescModal.replace(naviMIddleDescModal, naviMIddleDesc);
    //
    //    }
    //});

    $('#NaviHighModal').on('show.bs.modal', function () {
        var naviHighDescModal =  document.getElementById('naviHighDescModal').innerHTML;
        if (flag === 1) {
            $("#naviHightPriceModal").html(naviHighSubscription);
            $('#priceNaviHighModal').addClass('no-view');
            $('#priceNaviHighSubsModal').removeClass('no-view');
            document.getElementById('naviHighDescModal').innerHTML = naviHighDescModal.replace(naviHighDescModal, naviHighDescSub);
        } else {
            $("#naviHightPriceModal").html(naviHigh);
            $('#priceNaviHighSubsModal').addClass('no-view');
            $('#priceNaviHighModal').removeClass('no-view');
            document.getElementById('naviHighDescModal').innerHTML = naviHighDescModal.replace(naviHighDescModal, naviHighDesc);

        }
    });


    $(document).ready(function(){
        $('a[href^="#"]').on('click',function (e) {
            e.preventDefault();

            var target = this.hash;
            var $target = $(target);

            if ($target.offset()) {
                $('html, body').stop().animate({
                    'scrollTop': $target.offset().top
                }, 900, 'swing', function () {
                    window.location.hash = target;
                });
            }


        });
    });

});
