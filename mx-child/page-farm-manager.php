<?php
/**
 * Template Name: Farm management template
 *
 * @since MX 1.0
 */
get_header();

// get page layout
$layout = mx_get_page_layout();
$layout_class = mx_get_page_layout_class();

?>
    <div id="main" class="">
        <div class=" submenu hidden-sm hidden-xs">
            <div class="container">
                <div class="row " style="padding: 10px 0; border-top: 3px solid #ffffff;">
                    <p class="font-little light-grey light-weight col-sm-4 hidden-xs">Record keeping</p>

                    <div class="col-sm-8">
                        <ul class="list-inline font-little light-weight" style="position: relative; float:right">
                            <li class="text-left"><a href="#overview" class="light-grey">Overview</a></li>
                            <li class="text-left"><a href="#prices" class="light-grey ">Prices</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="hidden-sm hidden-xs placeholder"></div>
        <div class="header-bg-farm ">
            <div class="container ">
                <div class=" row">

                    <div class="col-md-5 col-sm-6" style="padding-top:90px">
                        <h1 class="main-color normal-weight font-large">Record keeping</h1>

                        <h3 class="main-color light-weight font-middle">Free field documentation</h3>

                        <p class="main-color light-weight font-little padding-both-30">Create and edit records of your
                            work when you are in the field.</p>

                        <div class="row" style="padding-bottom:90px">
                            <div class="col-md-4 col-xs-5 ">
                                <button type="button" class="btn btn-default btn-trial" aria-label="Left Align"
                                        style="background-color: #889c3b" >
                                    <a href="https://play.google.com/store/apps/details?id=com.kmware.efarmer&hl=ru" target="_blank">free download</a>
                                </button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="row green-bg">
            <span id="overview" class="link-document"> &nbsp;</span>

            <div class="container arrow_box  padding-both-30 ">
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
                        <div class="row">
                            <p class="text-center white-text font-22 light-weight">Free module that helps you to
                                document your work easily
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row padding-both-30">
                <div class="col-md-3 col-sm-6 text-center padding-both-30">
                    <img src="../wp-content/uploads/2015/10/manage-fields-pic.png"
                         alt="Create and manage field boundaries">

                    <p class="simple-p screen-decs">Create and manage field boundaries. </p>
                </div>
                <div class="col-md-3 col-sm-6 text-center padding-both-30">
                    <img src="../wp-content/uploads/2015/10/gps-tracking-pic.png" alt="calculate the covered area">

                    <p class="simple-p screen-decs">Use GPS tracking to calculate the covered area automatically.</p>
                </div>
                <div class="col-md-3 col-sm-6 text-center padding-both-30">
                    <img src="../wp-content/uploads/2015/10/material-input-pic.png"
                         alt="add the quantity of material used ">

                    <p class="simple-p screen-decs">Manually add the quantity of material used for the performed
                        operation.</p>
                </div>
                <div class="col-md-3 col-sm-6 text-center padding-both-30">
                    <img src="../wp-content/uploads/2015/10/report-pic.png" alt="field report">

                    <p class="simple-p screen-decs">View your field report and export it to PDF for your records. <a
                            href="#" class="green-link">See example</a></p>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <hr>
                <h3 class="font-20 light-weight grey-37  text-center padding-both-30">Check the detailed description of
                    the application functionality:</h3>

                <div class="col-md-4 col-md-offset-2 col-sm-6 col-xs-12 ">
                    <div class="features-block text-center">
                    <span class="fa-stack fa-lg features-icon">
                            <i class="fa fa-circle fa-stack-2x green-text"></i>
                            <i class="fa fa-map-o fa-stack-1x fa-inverse white-text"></i>
                        </span>
                        <h4 class="normal-weight grey-37  font-20">Maps</h4>

                        <p class="light-weight grey-37 ">Create and manage field boundaries. Store information and view the
                            progress of
                            your work on multiple fields. </p>
                        <a href="http://localhost:8888/efarmer/features/" class="opposite-text">Learn more </a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12 ">
                    <div class="features-block text-center">
                        <div class="green-circle">
                            <i class="material-icons white-icon">library_books</i>
                        </div>
                        <h4 class="normal-weight grey-37  font-20">Field records</h4>

                        <p class="light-weight grey-37 ">Make records of your field work directly in the field. Save
                            time on paperwork. Store and analyse information about your work. Export, share & print
                            field reports. </p>
                        <a href="http://localhost:8888/efarmer/features/" class="opposite-text">Learn more </a>
                    </div>
                </div>


                <div class="col-md-4 col-md-offset-2 col-sm-6 col-xs-12 ">
                    <div class="features-block text-center">
                        <div class="green-circle">
                            <i class="material-icons white-icon">devices</i>
                        </div>
                        <h4 class="normal-weight grey-37  font-20">Mobile and web </h4>

                        <p class="light-weight grey-37 ">Syncronise data between all your devices. </p>
                        <a href="http://localhost:8888/efarmer/features/" class="opposite-text">Learn more </a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12 ">
                    <div class="features-block text-center">
                        <div class="green-circle">
                            <i class="material-icons white-icon">cloud_queue</i>
                        </div>
                        <h4 class="normal-weight grey-37  font-20">Secure cloud</h4>

                        <p class="light-weight grey-37 ">All your data is securely saved in the cloud. </p>
                        <a href="http://localhost:8888/efarmer/features/" class="opposite-text">Learn more</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2">
                    <h3 class="font-20 light-weight grey-37  text-center padding-both-30">This solution is <b>available
                            for free </b>and can be supplemented by other premium eFarmer modules.</h3>
                </div>

            </div>
        </div>


        <div class="price-bg">
            <span id="prices" class="link-document"> &nbsp;</span>

            <div class="container">
                <div class="row">
                    <h2 class="font-32 grey-37 normal-weight text-center">eFarmer solution and paid modules</h2>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="price-block trial-price-block farm">
                            <h4 class="text-center bold-weight font-20 white-text shadow"> Record keeping </h4>

                            <p class="text-center price-label bold-weight shadow"
                               style="color: rgba(255, 255, 255, 0.75)"> FREE</p>

                            <p class="text-center note-text"> € 0 forever</p>

                            <div class="padding-both-30 font-16">
                                <ul>
                                    <li>Maps</li>
                                    <li>Field records</li>
                                    <li>Mobile & Web app</li>
                                    <li>Secured cloud</li>
                                </ul>
                            </div>
                            <div class="abs-pos">
                                <button class="btn btn-action-select"
                                        type="submit">
                                    <a href="https://play.google.com/store/apps/details?id=com.kmware.efarmer&hl=ru" target="_blank">download</a>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="price-block free-block farm">
                            <h4 class="text-center bold-weight font-20 white-text shadow"> Team management </h4>

                            <p class="text-center note-text"> coming in 2016 </p>

                            <p class="text-center price-label bold-weight shadow farm"> SOON</p>

                            <div class="padding-both-30 font-16">
                                <ul>
                                    <li>Includes everything in Record Kepping </li>
                                    <li>Team management functionality</li>
                                </ul>
                            </div>
                            <div class="abs-pos">
                                <button class="btn btn-action-select" type="submit" data-toggle="modal" data-target="#contactUs">contact us
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="price-block middle-price-block farm">
                            <h4 class="text-center bold-weight font-20 white-text shadow"> Fertiliser balance </h4>

                            <p class="text-center note-text"> coming in 2016 </p>

                            <p class="text-center price-label bold-weight shadow farm"> SOON</p>

                            <div class="padding-both-30 font-16">
                                <ul>
                                    <li>Includes everything in Record Kepping</li>
                                    <li>Fertiliser balance functionality </li>
                                </ul>
                            </div>
                            <div class="abs-pos">
                                <button class="btn btn-action-select" type="submit" data-toggle="modal" data-target="#contactUs"> contact us
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="price-block high-price-block farm">
                            <h4 class="text-center bold-weight font-20 white-text shadow"> Farm data analytics </h4>

                            <p class="text-center note-text"> coming in 2016 </p>

                            <p class="text-center price-label bold-weight shadow farm">SOON</p>


                            <div class="padding-both-30 font-16">
                                <ul>
                                    <li>Includes everything in Record Kepping</li>
                                    <li>Farm data analytics </li>
                                </ul>
                            </div>
                            <div class="abs-pos">

                                <button class="btn btn-action-select" type="submit" data-toggle="modal" data-target="#contactUs"> contact us
                                </button>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tractor-bg">
            <p class="font-32 text-center white-text padding-both-30 shadow">Save time with automatic field
                documentation</p>
        </div>
        <div class="all-devices-bg">
            <div class="container">
                <div class="row">
                    <h3 class="font-32 normal-weight main-text text-center" style="margin: 30px;">Information available
                        on all your devices</h3>

                    <div class="col-md-9 col-md-offset-2 col-sm-offset-4 col-sm-8 col-xs-12">
                        <img src="../wp-content/uploads/2015/10/alldevices-pic.png" alt="">
                    </div>


                </div>
            </div>
        </div>
        <div class="reports-bg">
            <p class="font-32 text-center main-color padding-both-30"> Copy, print, share your field reports without any
                restrictions. </p>
        </div>
        <!--                    START contact us modal-->
        <div class="modal fade" tabindex="-1" id="contactUs" role="dialog">
            <div class="modal-dialog modal-md" style="margin-top: 200px">
                <div class="modal-content row">
                    <div class="col-xs-12">
                        <div class="row green-bg contact">
                            <div class="">
                                <div class="row">
                                    <h2 class="text-center white-text font-22 normal-weight">Contact us
                                    </h2>
                                </div>
                            </div>
                        </div>

                        <div class="contacts-block">
                            <p class="font-little dark-grey-color light-weight"><i
                                    class="fa fa-envelope"></i> info@efarmer.mobi </p>

                            <p class="font-little dark-grey-color light-weight"><i
                                    class="fa fa-skype"></i>
                                eFarmer.Support</p>

                            <p class="font-little dark-grey-color light-weight"><i
                                    class="fa fa-phone"></i>
                                +31 713 020355</p>
                        </div>
                        <button class="btn btn-action-select contact"
                                type="submit" data-toggle="modal" data-target="#contactUs">
                            <a href="mailto:oridyy@gmail.com?Subject=Support">Send e-mail </a>
                        </button>

                    </div>
                </div>
            </div>
            <!--                    END contact us modal-->

    </div>
<?php get_footer(); ?>