<?php wp_enqueue_script('scripts', '/wp-content/themes/mx-child/scripts.js', array('jquery'), false, false); ?>
<?php load_theme_textdomain('mx-child', get_template_directory() . './'); ?>


<?php
function my_theme_add_editor_styles() {
    add_editor_style( 'bootstrap.min.css' );
    add_editor_style( 'style.css' );
}
add_action( 'admin_init', 'my_theme_add_editor_styles' );
?>

<?php
add_filter( 'woocommerce_billing_fields', 'wc_npr_filter_state_billing', 10, 1 );
add_filter( 'woocommerce_shipping_fields', 'wc_npr_filter_state_shipping', 10, 1 );

function wc_npr_filter_state_billing( $address_fields ) {

$address_fields['billing_state']['required'] = false;
return $address_fields;
}
function wc_npr_filter_state_shipping( $address_fields ) {

$address_fields['shipping_state']['required'] = false;
return $address_fields;
}
add_filter( 'allow_subdirectory_install', create_function( '', 'return true;' ) );

?>


<?php
// Remove "Choose and option" in variable product
add_filter( 'woocommerce_dropdown_variation_attribute_options_args', 'mmx_remove_select_text'); function mmx_remove_select_text( $args ){ $args['show_option_none'] = ''; return $args; }

// Remove price range indication from variable product

add_filter('woocommerce_variable_price_html', 'custom_variation_price', 10, 2);
function custom_variation_price( $price, $product ) {
$price = '';
if ( !$product->min_variation_price || $product->min_variation_price !== $product->max_variation_price ) $price .= '<span class="from">' . _x('From', 'min_price',  'woocommerce') . ' </span>';
$price .= woocommerce_price($product->min_variation_price);
$price .= '<small class="woocommerce-price-suffix">&nbsp;ex. VAT</small>';
return $price;
}

?>
