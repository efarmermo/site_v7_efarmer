<?php
/**
 * Template Name: Features template
 *
 * @since MX 1.0
 */
get_header();

// get page layout
$layout = mx_get_page_layout();
$layout_class = mx_get_page_layout_class();

?>
    <div id="main" class="" style="background-color: #f6f8f9">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2 class="font-32 blue-text normal-weight text-up">Maps </h2>

                    <h3 class="font-22 grey-37 normal-weight margin-bot-30 ">Creating and managing field
                        boundaries. </h3>

                    <p class="features-p light-weight margin-bot-30 "><strong>Import fields.</strong> ou can import your
                        own fields in the .SHP format in the web version of our application. Drag and drop the available
                        cadaster document. </p>

                    <p class="features-p light-weight"><strong>Draw fields.</strong> You can draw the boundaries of your
                        fields in eFarmer application manually using Google Maps. While working with eFarmer all
                        information from your field works will be assigned to the particular field, kept in the secured
                        cloud and available at any time from any device.</p>
                </div>
                <div class="col-md-6">
                    <img src="../wp-content/uploads/2015/11/maps-feature.png" class="img-features" alt="">
                </div>
            </div>
            <hr>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <img src="../wp-content/uploads/2015/11/record-keeping-feature.png" class="img-features" alt="">

                </div>
                <div class="col-md-6">
                    <h2 class="font-32 blue-text normal-weight text-up">RECORD KEEPING</h2>

                    <h3 class="font-22 grey-37 normal-weight margin-bot-30 ">Record keeping and field reports. </h3>

                    <p class="features-p light-weight"><strong>Field records.</strong>You will be able to input
                        following data manually while working in the field:
                    </p>
                    <ul class="features-p light-weight margin-bot-30 ">
                        <li>type of operation</li>
                        <li>denomination and quantity of materials used (fertilizers, pesticides, seeds, etc.) </li>
                    </ul>

                    <p class="features-p light-weight"><strong>Automatic tracking.</strong>The following information can
                        be added automatically to the field report from the GPS tracking:</p>
                    <ul class="features-p light-weight margin-bot-30 ">
                        <li>type of operation</li>
                        <li>area and distance covered </li>
                        <li>machine used </li>
                    </ul>
                    <p class="features-p light-weight"><strong>Reports.</strong> As a result you will be able to view
                        your field report (see example) and export it to PDF for your records.
                        <a href="" class="green-link">See example report.</a></p>
                </div>

            </div>
            <hr>
        </div>

        <div class="container">
            <div class="row">
                <h2 class="font-32 blue-text normal-weight text-up">FIELD GUIDANCE</h2>

                <h3 class="font-22 grey-37 normal-weight margin-bot-30 ">Explore precision farming with parallel
                    driving</h3>

                <p class="features-p light-weight">eFarmer navigation application is a GPS guidance system to be used on
                    your Android smartphone and/or tablet. Checking the display while driving will enable you to perform
                    field operations in parallel and evenly spaced lines. Thus to reduce overlaps and missed spots. In
                    our system you are able to have the following navigation patterns:  

                </p>

                <div class="col-md-6 margin-bot-30 ">

                    <h3 class="font-20 grey-37 normal-weight margin-bot-30  text-center">Straight AB</h3>
                    <img src="../wp-content/uploads/2015/11/tablet-abstraight.png" class="img-features  text-center"
                         alt="">

                    <p class="features-p light-weight"><strong>Straight АВ</strong> is suitable for the regular shaped
                        square or
                        rectangular fields, without “non operational spots”. In this case our system will automatically
                        draw  parallel straight lines that you should follow on your field. </p>


                </div>
                <div class="col-md-6 margin-bot-30 ">
                    <h3 class="font-20 grey-37 normal-weight margin-bot-30  text-center">AB Curve</h3>
                    <img src="../wp-content/uploads/2015/11/tablet-abcurve.png" class="img-features  text-center"
                         alt="">

                    <p class="features-p light-weight">
                        <strong>АВ curve</strong> is suitable for all field’s
                        shapes and particularly for fields with “non operational spots”. In this case you will make the
                        first run along a curved/non-straight line, after the first turn the application will create
                        next
                         lines  parallel to the first run.  
                    </p>
                </div>
                <p class="features-p light-weight text-center">The distinctive characteristic of eFarmer navigation
                    system is that
                    it  combines tractor guidance with farm management. You will not only make parallel lines, but your
                    operation’s details such as area that you covered, type of operation, distance and time spent in the
                    field, materials used will be automatically added to your field report and stored in the  secured
                    cloud.
            </div>
            <hr>
        </div>

        <div class="container">
            <div class="row">
                <h2 class="font-32 blue-text normal-weight text-up">MOBILE AND WEB </h2>

                <h3 class="font-22 grey-37 normal-weight margin-bot-30 ">Synchronise data between all your devices</h3>

                <p class="features-p light-weight">With eFarmer you will be able to insert information about your
                    operations directly in the field and work with this data later on from your office. 
                </p>

                <div class="col-md-4 margin-bot-30 ">
                    <div class="same-height">
                        <img src="../wp-content/uploads/2015/11/desktop-feature.png" class="img-features  text-center"
                             alt="">
                    </div>

                    <h3 class="font-20 grey-37 normal-weight margin-bot-30  text-center">Desktop</h3>


                    <p class="features-p light-weight">Login on eFarmer website and see your records on a big screen in
                        your office. You will be able to download, copy and share your data without any restrictions.
                    </p>
                </div>
                <div class="col-md-4 margin-bot-30 ">
                    <div class="same-height">
                        <img src="../wp-content/uploads/2015/11/tablet-feature.png" class="img-features  text-center"
                             alt="">
                    </div>

                    <h3 class="font-20 grey-37 normal-weight margin-bot-30  text-center">Tablet</h3>


                    <p class="features-p light-weight">Tablet is more practical to use for field farm tasks. The bigger
                        screen and no phone calls will enable you to achieve better results during your work.   
                    </p>
                </div>
                <div class="col-md-4 margin-bot-30 ">
                    <div class="same-height">
                        <img src="../wp-content/uploads/2015/11/phone-feature.png" class="img-features  text-center"
                             alt="">
                    </div>

                    <h3 class="font-20 grey-37 normal-weight margin-bot-30  text-center">Smartphone</h3>


                    <p class="features-p light-weight">You can use your Android smartphone to insert data into the
                        system while working in the field with farm management and navigation systems.     
                    </p>
                </div>
                <p class="features-p light-weight margin-bot-30"><strong>eFarmer is</strong> compatible with all Android
                    smartphones
                    and tablets. But the quality of work with the system may be impaired due to your device specific
                    characteristics such as low GPS signal. To get best experience from the eFarmer,
                    <a href="https://efarmer.atlassian.net/wiki/display/SUPP/eFarm+Pilot+GPS+Field+Guidance+Compatible+Devices+List"
                       target="_blank" class="green-link">
                        check the list of recommended devices.</a></p>

                <p class="features-p light-weight margin-bot-30 ">You are able to work with the system even without cell
                    service. Use
                    “offline” mode of application and all information will be added automatically to your cloud once
                    you’ll get a connection. Although while performing parallel driving it is not recommended to make
                    phone calls from your device as it can lower the stability of GPS signal. To improve the accuracy of
                    the system we advise you to purchase an external antenna. <a
                        class="green-link" href="http://localhost:8888/efarmer/tractor-guidance/"> Check our navigation
                        system
                        solutions.</a></p>

                <p class="features-p light-weight">Working with smartphone and tablet in the cabin will require  certain
                    peripherals, such as:</p>
                <ul class="features-p light-weight margin-bot-30 ">
                    <li>Connector cable for consumer smartphones and tablets (USB-OTG cable).</li>
                    <li>USB extension cable for GPS.</li>
                    <li>In-vehicle USB power.</li>
                    <li>Holder for device.</li>
                </ul>
                <p class="features-p light-weight">Check our <a href="#" class="green-link">Install&Explore guide</a>
                    for more details. Our support team will be happy to assist you with
                    finding and purchasing right peripherals without any extra charges.</p>
            </div>
            <hr>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2 class="font-32 blue-text normal-weight text-up">SECURE CLOUD </h2>

                    <h3 class="font-22 grey-37 normal-weight margin-bot-30 ">You own your data, and we keep it secure,
                        private, and protected. </h3>

                    <p class="features-p light-weight margin-bot-30 ">All your data is securely stored in the cloud and
                        available at any time from any device. Our servers are located at
                        <a href="https://www.hetzner.de/" target="_blank" class="green-link">Hetzner Online</a> in the
                        Federal
                        Republic of Germany.</p>
                </div>
                <div class="col-md-6">
                    <img src="../wp-content/uploads/2015/11/cloud-feature.png" class="img-features" alt="">
                </div>
            </div>
            <hr>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <img src="../wp-content/uploads/2015/11/management-feature.png" class="img-features" alt="">
                </div>
                <div class="col-md-8">
                    <h2 class="font-32 blue-text normal-weight text-up">TEAM MANAGEMENT</h2>

                    <h3 class="font-22 grey-37 normal-weight margin-bot-30 ">Task assigning and collaboration.</h3>

                    <p class="features-p light-weight margin-bot-30 ">The goal of the team management functionality is
                        to help farmer to manage its  employees and coordinate your work with neighbors and colleagues
                        while performing collaborative tasks. </p>

                    <p class="features-p light-weight margin-bot-30 ">The information about work performed will be added
                        automatically to your field record. The number of users in the system is unlimited. We are
                        currently working on development of this functionality and inviting you to become our
                        co-creator. </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <h2 class="font-20 grey-37 normal-weight margin-bot-30 text-up text-center">YOU WILL BE ABLE
                        TO:</h2>
                    <ul class="features-p light-weight margin-bot-30 ">
                        <li>Choose the type of operation such as harvesting, seeding, fertilising, spraying, tilling,
                            scouting and add your own type of work.
                        </li>
                        <li>Choose the field where it should be done.</li>
                        <li>Find the worker/neighbor from your contact list.</li>
                        <li>Send him the task description in the application or via email.</li>
                        <li>See the progress of the wok.</li>
                        <li>Accept the work.</li>
                        <li>Chat in the application.</li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <h2 class="font-20 grey-37 normal-weight margin-bot-30 text-up text-center">YOUR WORKER WILL BE ABLE
                        TO:</h2>
                    <ul class="features-p light-weight margin-bot-30 ">
                        <li>Accept the task.</li>
                        <li>Create report about work performed and send it to you.</li>
                        <li>Send you an invoice.</li>
                    </ul>
                </div>
            </div>
            <hr>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2 class="font-32 blue-text normal-weight text-up">FARM DATA ANALYTICS </h2>

                    <h3 class="font-22 grey-37 normal-weight margin-bot-30 ">Data driven approach in farm
                        management. </h3>

                    <p class="features-p light-weight">Don’t rely only on your gut feelings – start use
                        hard data in making decisions. (Get competitive advantage by using data in managing your farm
                        business). </p>

                    <p class="features-p light-weight margin-bot-30 "> Input your activities, materials, fields and,
                        basically, any important information. eFarmer will
                        save it in the cloud, analyse the data and create a number of reports, such as:</p>

                    <ul class="features-p light-weight margin-bot-30 ">
                        <li>Field work summary report. See all your field operations and how many hectares were
                            covered.
                        </li>
                        <li>Material summary report. History of all material inputs for a given period, per operation,
                            field, input quantity.
                        </li>
                        <li>Plan-fact analysis  to see the difference.</li>
                        <li>Vehicle summary report. See how much time you have used your tractor per each operation.
                        </li>
                        <li>Create your own templates of reports.</li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <img src="../wp-content/uploads/2015/11/maps-feature.png" class="img-features" alt="">
                </div>
            </div>
            <p class="features-p light-weight margin-bot-30 ">Use this data for Government compliance reports. <br>
                The more information you insert, the more eFramer functionalities you use -  the better results you will
                get! We are currently working on development of this functionality and
                <a href="#" class="green-link">inviting you to become our co-creator</a>.</p>
            <hr>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <img src="../wp-content/uploads/2015/11/fertilisercalc-feature.png" class="img-features" alt="">
                </div>
                <div class="col-md-6">
                    <h2 class="font-32 blue-text normal-weight text-up">FERTILISER BALANCE </h2>

                    <h3 class="font-22 grey-37 normal-weight margin-bot-30 ">Control your chemicals. </h3>

                    <p class="features-p light-weight margin-bot-30 ">eFarmer will allow you to manage fertilisers
                        easily with a built-in calculator. To start to work with the system you will have to input
                        information about existing chemicals level and type of crops on your fields. This functionality
                        will enable you to: </p>
                    <ul class="features-p light-weight margin-bot-30 ">
                        <li>choose the type of pesticides and fertilizers materials from the list, and add your own
                            items;
                        </li>
                        <li>calculate the amount of chemicals needed for particular field according to the norms ; 

                        </li>
                        <li>add automatically the information about performed work to your field report;</li>
                        <li>create an alarm when the amount of chemicals is  close to the critical  level.
                        </li>
                    </ul>
                </div>
            </div>
            <p class="features-p light-weight margin-bot-30 ">Use this data for Government compliance reports. We are
                currently working on development of this
                functionality and <a href="#" class="green-link">inviting you to become our co-creator</a>.</p>

            <hr>
        </div>


        <!--        START Compatible Devices List modal-->

        <div class="modal fade" tabindex="-1" id="devices" role="dialog">
            <div class="modal-dialog modal-mg" style="margin-top: 200px">
                <div class="modal-content row all-padding">
                    <p class="font-middle grey-37 margin-both-20">
                        Compatible Devices List
                    </p>

                    <p class="simple-p margin-both-20 bold-weight ">List of Compatible Devices</p>
                    <ol class="simple-p list-devices ">
                        <li>Samsung Tab Pro 8.4</li>
                        <li>Samsung Tab S 8.4 LTE</li>
                        <li>Samsung Tab 4 7</li>
                        <li>Lenovo A7-50/A3500 HV</li>
                        <li>Asus ME181CX Pad 8</li>
                    </ol>

                    <p class="simple-p margin-both-20 bold-weight ">List of Uncompatible Devices</p>
                    <ol class="simple-p list-devices">
                        <li>Lenovo A7-50/A3500 FL</li>
                        <li>Prestigio MultiPad Color 7.0 3G PMT 5777</li>
                        <li>Asus MeMO Pad HD 7 ME173X</li>
                    </ol>

                </div>
            </div>
        </div>

        <!--        END Compatible Devices List modal--->

    </div>
<?php get_footer(); ?>