<?php
/**
 * Template Name: Main Page
 *
 * @since MX 1.0
 */
get_header();

// get page layout
$layout = mx_get_page_layout();
$layout_class = mx_get_page_layout_class();

?>
    <div id="main" class="">

        <div class=" header-bg">
            <div class="container ">
                <div class=" row">

                    <div class="col-md-5 col-sm-6" style="padding-top:90px">
                        <h1 class="white-text normal-weight font-large shadow-hard">We are eFarmer</h1>

                        <h3 class="white-text light-weight font-middle">We offer tractor GPS guidance solution for your
                            smartphone.</h3>

                        <p class="white-text light-weight font-little padding-both-30">This mobile app visually assists
                            you in making the optimal passes on the field and documents your work. Now you can be more
                            efficient and professional while working in the tractor.</p>

                        <div class="row" style="padding-bottom:90px">
                            <div class="col-sm-5 col-xs-6">
                                <button type="button" class="btn btn-trial" aria-label="Left Align">
                                    <a href="https://play.google.com/store/apps/details?id=com.kmware.efarmer&hl=ru"
                                       target="_blank">FREE TRIAL</a>
                                </button>
                            </div>
                            <div class="col-sm-5 col-xs-6">
                                <button type="button" class="btn btn-buy" aria-label="Left Align">
                                    <a href="http://localhost:8888/efarmer/shop/">buy now</a>
                                </button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>

        <div class="row green-bg  ">
            <div class="container arrow_box">
                <div class="row">
                    <div class="col-sm-2 col-xs-12 text-center">
                        <div class="white-circle margin-both-30">
                            <i class="material-icons green-icon ">navigation</i>
                        </div>
                    </div>
                    <div class="col-sm-8 col-xs-12">
                        <div class="row">
                            <div class="col-xs-10 col-xs-offset-1">
                                <h2 class="text-center white-text font-32 normal-weight margin-both-30"
                                    style="padding-top: 8px;"><span
                                        class="underline"> What is eFarmer? </span>
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2 col-xs-12 text-center">
                        <div class="white-circle margin-both-30">
                            <i class="material-icons green-icon">folder</i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row padding-both-30">
                <div class="col-sm-4">
                    <h3 class="font-32 normal-weight opposite-text">Tractor guidance</h3>

                    <p class="main-color font-little-wide light-weight">With eFarmer navigation solutions you will be
                        more precise with

                    <p class="main-color font-little-wide light-weight"> With eFarmer navigation solutions you will be
                        more precise with your driving. With better driving you will improve the income per hectare by saving on
                        materials, your driving. With better driving you will improve income per hectare by saving on
                        materials, working hours and machine costs.</p>
                </div>
                <div class="col-sm-4">
                    <span class="vertical-divider-20"></span>

                    <div class="plus-block">
                        <div class="grey-plus">
                            <i class="material-icons grey-plus-color">add</i>
                        </div>

                        <p class="main-color font-little text-center">Free field record keeping system is at the heart
                            of our
                            service. With your smartphone you can easily capture and document all field activities,
                            build better records and make data-driven decisions. </p>
                    </div>

                    <span class="vertical-divider-40"></span>


                </div>
                <div class="col-sm-4">
                    <h3 class="font-32 normal-weight green-text">Farm management</h3>

                    <p class="main-color font-little-wide light-weight">You can further increase the efficiency of your farm
                        business by using
                        other paid eFarmer farm management solutions such as  team management, fertiliser calculator and
                        farm data analysis.

                    </p>
                </div>
            </div>
            <hr>
            <p class="text-center font-22 col-sm-10 col-sm-offset-1 main-color light-weight">
                Our navigation solutions help you to spend less on materials while increasing your yield with better driving
            </p>
        </div>


        <div class="all-devices-bg">
            <div class="container">
                <div class="row">
                    <h3 class="font-32 normal-weight main-text text-center grey-37" style="margin: 30px;">For all your
                        devices</h3>

                    <div class="col-md-3 col-sm-4 col-xs-11 col-xs-offset-1 col-sm-offset-0 padding-both-30">
                        <p class="font-little dark-grey-color light-weight"><i
                                class="material-icons font-35">desktop_windows</i>
                            <span class="list-device">PC or MAC</span></p>

                        <p class="font-little dark-grey-color light-weight">

                            <img src="wp-content/themes/mx-child/fonts/iconfont/tabletandphone.svg"
                                 class="svg icon-calc" id="tablet"/>
                            <span class="list-device" style="left: 7px">Android OS</span>
                        </p>

                        <p class="font-little dark-grey-color light-weight half-opacity">

                            <i class="material-icons font-35">phone_iphone</i>
                            <span class="list-device">iOS coming soon</span>
                        </p>
                    </div>

                    <div class="col-md-9 col-sm-8 col-xs-12">
                        <img src="wp-content/uploads/2015/10/alldevices-pic.png" alt="">
                    </div>


                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <h3 class="font-32 normal-weight main-text text-center grey-37">Main features</h3>

                <div class="col-md-4 col-sm-6 col-xs-12 ">
                    <div class="features-block text-center">
                        <div class="green-circle">
                            <i class="material-icons white-icon">map</i>
                        </div>
                        <h4 class="normal-weight grey-37 font-20">Maps</h4>

                        <p class="light-weight grey-37">Create and manage field boundaries. Store information and view the
                            progress of
                            your work on multiple fields. </p>
                        <a href="http://localhost:8888/efarmer/features/" class="opposite-text">Learn more </a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12 ">
                    <div class="features-block text-center">
                        <div class="green-circle">
                            <i class="material-icons white-icon">library_books</i>
                        </div>
                        <h4 class="normal-weight grey-37 font-20">Field records</h4>

                        <p class="light-weight grey-37">Make records of your fieldwork directly in the field. Save
                            time on paperwork. Store and analyse information about your work. Export, share & print
                            field reports. </p>
                        <a href="http://localhost:8888/efarmer/features/" class="opposite-text">Learn more </a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12 ">
                    <div class="features-block text-center">
                        <div class="green-circle">
                            <i class="material-icons white-icon">navigation</i>
                        </div>
                        <h4 class="normal-weight grey-37 font-20">Field guidance</h4>

                        <p class="light-weight grey-37">Checking the display while driving will enable you to perform
                            field operations in parallel and evenly spaced lines. Thus reducing overlaps and missed
                            spots. </p>
                        <a href="http://localhost:8888/efarmer/features/" class="opposite-text">Learn more </a>
                    </div>
                </div>


                <div class="col-md-4 col-sm-6 col-xs-12 ">
                    <div class="features-block text-center">
                        <div class="green-circle">
                            <i class="material-icons white-icon">devices</i>
                        </div>
                        <h4 class="normal-weight grey-37 font-20">Mobile and web </h4>

                        <p class="light-weight grey-37">Synchronize data between all your devices. Access your data conveniently from anywhere</p>
                        <a href="http://localhost:8888/efarmer/features/" class="opposite-text">Learn more </a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12 ">
                    <div class="features-block text-center">
                        <div class="green-circle">
                            <i class="material-icons white-icon">people</i>
                        </div>
                        <h4 class="normal-weight grey-37 font-20">Team management </h4>

                        <p class="light-weight grey-37"> eFarmer will help you to manage your employees and coordinate
                            with your neighbors and colleagues while performing collaborative tasks. </p>
                        <a href="http://localhost:8888/efarmer/features/" class="opposite-text">Learn more </a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12 ">
                    <div class="features-block text-center">
                        <div class="green-circle">
                            <i class="material-icons white-icon">iso</i>
                        </div>
                        <h4 class="normal-weight grey-37 font-20">Fertiliser balance</h4>

                        <p class="light-weight grey-37">eFarmer will allow you to manage fertilisers easily with a
                            built-in calculator. </p>
                        <a href="http://localhost:8888/efarmer/features/" class="opposite-text">Learn more </a>
                    </div>
                </div>


                <div class="col-md-4 col-sm-6 col-xs-12 ">
                    <div class="features-block text-center">
                        <div class="green-circle">
                            <i class="material-icons white-icon">settings_input_antenna</i>
                        </div>
                        <h4 class="normal-weight grey-37 font-20">External receiver</h4>

                        <p class="light-weight grey-37">Reduce the width of overlaps down to 20 cm with high
                            positioning accuracy with our GPS receiver. </p>
                        <a href="http://localhost:8888/efarmer/features/" class="opposite-text">Learn more </a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12 ">
                    <div class="features-block text-center">
                        <div class="green-circle">
                            <i class="material-icons white-icon">trending_up</i>
                        </div>
                        <h4 class="normal-weight grey-37 font-20">Farm data analytics </h4>

                        <p class="light-weight grey-37"> Don’t rely only on your gut feelings! Start using hard data
                            in making decisions. </p>
                        <a href="http://localhost:8888/efarmer/features/" class="opposite-text">Learn more </a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12 ">
                    <div class="features-block text-center">
                        <div class="green-circle">
                            <i class="material-icons white-icon">cloud_queue</i>
                        </div>
                        <h4 class="normal-weight grey-37 font-20">Secure cloud</h4>

                        <p class="light-weight grey-37">All your data is securely saved in the cloud. </p>
                        <a href="http://localhost:8888/efarmer/features/" class="opposite-text">Learn more </a>
                    </div>
                </div>


            </div>
        </div>

        <div class="best-price">
            <h3 class="text-center white-text font-32 normal-weight padding-both-30 shadow"> Best price / quality
                solution
                for
                small and medium farm business </h3>

            <div class="row text-center">
                <div class="euro-pic"></div>
                <div class="plus-pic"></div>
                <div class="like-pic"></div>

            </div>
            <div style="height: 400px;"></div>

        </div>
        <div class="row green-bg">
            <div class="container padding-both-30 arrow_box">
                <div class="row">

                    <div class="col-sm-12 col-xs-10 col-sm-offset-0 col-xs-offset-1">
                        <h2 class="text-center white-text font-32 normal-weight"><span
                                class="underline">Our solutions </span>
                        </h2>

                        <p class="text-center white-text font-22 light-weight">Flexible offers for your specific
                            needs </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="price-bg">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4 col-xs-12">
                        <div class="price-block free-block">
                            <h4 class="text-center bold-weight font-20 white-text shadow"> Record keeping </h4>

                            <p class="text-center price-label bold-weight shadow">€ 0 </p>

                            <p class="text-center note-text"> Free forever </p>

                            <div class="padding-both-30 font-16">
                                <p> This package includes:</p>
                                <ul>
                                    <li> Maps</li>
                                    <li> Field records</li>
                                    <li> Mobile and Web app</li>
                                    <li> Secure cloud</li>
                                </ul>
                            </div>
                            <div class="abs-pos">

                                <button class="btn btn-action-select"
                                        type="submit">
                                    <a href="https://play.google.com/store/apps/details?id=com.kmware.efarmer&hl=ru"
                                       target="_blank">try now</a>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <div class="price-block middle-price-block">
                            <h4 class="text-center bold-weight font-20 white-text shadow"> Navigation</h4>

                            <p class="text-center price-label bold-weight shadow">€ 249 </p>

                            <p class="text-center note-text"> One off payment </p>

                            <div class="padding-both-30 font-16">
                                <p> This package includes:</p>
                                <ul>
                                    <li> Everything in <b> Record keeping </b></li>
                                    <li> Tractor guidance</li>
                                    <li><u> External receiver </u> (optional)</li>
                                </ul>
                            </div>
                            <div class="abs-pos">
                                <button class="btn btn-action-select"
                                        type="submit">
                                    <a href="http://localhost:8888/efarmer/shop/">buy now</a>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <div class="price-block high-price-block">
                            <h4 class="text-center bold-weight font-20 white-text shadow"> Farm management </h4>

                            <p class="text-center price-label bold-weight shadow"
                               style="color: rgba(255, 255, 255, 0.75)"> SOON</p>

                            <p class="text-center note-text"> coming in 2016 </p>

                            <div class="padding-both-30 font-16">
                                <p> This package includes:</p>
                                <ul>
                                    <li> Everything in <b>Record keeping </b></li>
                                    <li> Team management</li>
                                    <li> Fertiliser balance</li>
                                    <li> Analytics</li>
                                </ul>
                            </div>
                            <div class="abs-pos">
                                <button class="btn btn-action-select"
                                        type="submit" data-toggle="modal" data-target="#contactUs"> contact us
                                </button>
                            </div>

                        </div>
                    </div>

                    <!--                    START contact us modal-->
                    <div class="modal fade" tabindex="-1" id="contactUs" role="dialog">
                        <div class="modal-dialog modal-md" style="margin-top: 200px">
                            <div class="modal-content row">
                                <div class="col-xs-12">
                                    <div class="row green-bg contact">
                                        <div class="">
                                            <div class="row">
                                                <h2 class="text-center white-text font-22 normal-weight">Contact us
                                                </h2>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="contacts-block">
                                        <p class="font-little dark-grey-color light-weight"><i
                                                class="fa fa-envelope"></i> info@efarmer.mobi </p>

                                        <p class="font-little dark-grey-color light-weight"><i
                                                class="fa fa-skype"></i>
                                            eFarmer.Support</p>

                                        <p class="font-little dark-grey-color light-weight"><i
                                                class="fa fa-phone"></i>
                                            +31 713 020355</p>
                                    </div>
                                    <button class="btn btn-action-select contact"
                                            type="submit" data-toggle="modal" data-target="#contactUs">
                                        <a href="mailto:oridyy@gmail.com?Subject=Support">Send e-mail </a>
                                    </button>

                                </div>
                            </div>
                        </div>
                        <!--                    END contact us modal-->

                    </div>
                </div>
            </div>
        </div>
        <div class="video-wrapper" id="video">

            <iframe width="100%" height="100%" src="https://www.youtube.com/embed/B-zbylzQm1s"
                    frameborder="0"
                    allowfullscreen></iframe>
        </div>
        <div>
            <div class="continer">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h3 class="font-large normal-weight"><span
                                class="underline-dark"> 21,432 farmers </span></h3>

                        <p class="dark-grey-text font-little"> have improved their work with eFarmer </p>
                    </div>
                </div>
                <div id="carousel-slider" class="carousel slide" data - ride="carousel">

                    <!--Wrapper for slides-->
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="image-people"></div>
                                        <p class="review">“I have been  using the GPS receiver for a few weeks and
                                            now I
                                            can’t do
                                            without
                                            it. The eFarmer Navi is very helpful when fertilizing grasslands. Both
                                            in
                                            daylight, and even in the dark, you can easily find routes and record
                                            the
                                            finished area.”</p>

                                        <p class="review">
                                            <b> André Hornberg </b>, Germany
                                        </p>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="image-people"></div>
                                        <p class="review">“Compared to other field guidance solutions, eFarmer Navi
                                            makes it much easier to follow the tracks. The application itself is
                                            very easy to learn and use. A big advantage is that the Android tablet may
                                            be
                                            used for for other purposes as well.”

                                        </p>

                                        <p class="review">
                                            <b> Vsnyei Tamas </b>, Hungary
                                        </p>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="image-people"></div>
                                        <p class="review">“I have been using the eFarmer app since autumn 2014 and have started
                                            using the eFarmer Navi a few months ago. It is great being able to have up-to-date
                                            data of my works stored in one place. We also use the GPS receiver for
                                            spraying and fertilizing jobs.”
                                        </p>

                                        <p class="review">
                                            <b> Vadasz Janos </b>, Hungary
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="image-people"></div>
                                        <p class="review">“I have been  using the GPS receiver for a few weeks and
                                            now I
                                            can’t do
                                            without
                                            it. The eFarmer Navi is very helpful when fertilizing grasslands. Both
                                            in
                                            daylight, and even in the dark, you can easily find routes and record
                                            the
                                            finished area.”</p>

                                        <p class="review">
                                            <b> André Hornberg </b>, Germany
                                        </p>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="image-people"></div>
                                        <p class="review">“Compared to other field guidance solutions, eFarmer Navi
                                            makes it much easier to follow the tracks. The application itself is
                                            very easy to learn and use. A big advantage is that the Android tablet may
                                            be
                                            used for for other purposes as well.”

                                        </p>

                                        <p class="review">
                                            <b> Vsnyei Tamas </b>, Hungary
                                        </p>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="image-people"></div>
                                        <p class="review">“I have been using the eFarmer app since autumn 2014 and have started
                                            using the eFarmer Navi a few months ago. It is great being able to have up-to-date
                                            data of my works stored in one place. We also use the GPS receiver for
                                            spraying and fertilizing jobs.”
                                        </p>

                                        <p class="review">
                                            <b> Vadasz Janos </b>, Hungary
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Controls -->
                    <a class="left carousel-control" href="#carousel-slider" role="button" data-slide="prev">
                        <i class="material-icons controls" aria-hidden="true"> keyboard_arrow_left</i>
                        <span class="sr-only"> Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-slider" role="button" data-slide="next">
                        <i class="material-icons controls" aria-hidden="true"> keyboard_arrow_right</i>
                        <span class="sr-only"> Next</span>
                    </a>

                </div>
            </div>
        </div>

    </div>
<?php get_footer(); ?>